﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
let passwdMatch = false, passwdLength = true;
let passwd1 = "", passwd2 = "";

function passwdCheck() {
    if (passwd1 === passwd2) {
        passwdMatch = true;
    } else {
        passwdMatch = false;
    }

/*    if (passwd1.length > 7 && passwd2.length > 7) {
        passwdLength = true;
    } else {
        passwdLength = false;
    }*/
}

$("#pswdField1").on("change", () => {
    passwd1 = $("#pswdField1").val();
    passwdCheck();
})

$("#pswdField2").on("change", () => {
    passwd2 = $("#pswdField2").val()
    passwdCheck()
})

$("#btnResetPassword").on("click", () => {
    if (passwdMatch && passwdLength) {
        let err = ""
        $("#errorMessage").text(err)
        let pNr = $("#identity").val()

        let sendData = JSON.parse('{' + '"id": "' + pNr + '",' + '"password": "' + passwd1 + '"' + '}')
        /*        console.log(data)
        */        //ajax message to /api/users

        // var settings = {
        //     "async": true,
        //     "crossDomain": true,
        //     // "url": "https://localhost:44330/Users/User?=" + JSON.stringify(sendData),
        //     "url": "https://localhost:3000/?=" + JSON.stringify(sendData),
        //     "method": "POST",
        //     "headers": {
        //     "Content-Type": "application/x-www-form-urlencoded",
        //     "Accept": "*",
        //     "Cache-Control": "no-cache",
        //     // "Host": "localhost:44330",
        //     "Host": "localhost:3000",
        //     "Accept-Encoding": "gzip, deflate",
        //     "Content-Length": "",
        //     "Connection": "keep-alive",
        //     "cache-control": "no-cache"
        //     }
        // }

        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://localhost:3000/",
            "method": "POST",
            "headers": {
              "data": JSON.stringify(sendData),
            }
        }

        $.ajax(settings).done(function (response) {
            console.log(response);
        });

    } else {
        let err = ""
        if (!passwdMatch) {
            err = "Passwords not matching! "
        }
        if (!passwdLength) {
            err += "Password too short!"
        }
        $("#errorMessage").text(err)
    }
})