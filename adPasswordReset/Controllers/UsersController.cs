﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

//not in use currently, leaving in case of upgrading to asp.net core 3.0
namespace adPasswordReset.Controllers
{
    public class UsersController : Controller
    {
        public IActionResult PasswordCheck()
        {
            ViewData["Message"] = "Check if the password matches on this page.";

            return View();
        }

        //Would probably take ID as an input here and send it throught View Data.
        public IActionResult PasswordReset()
        {
            ViewData["Message"] = "Reset your password on this page.";

            return View();
        }

        //Handling this with NodeJS instead.
        // [HttpPost]
        // public new void User(string json)
        // {
        //     Debug.WriteLine("Received input. Identity: " + json);
        // }
    }
}
